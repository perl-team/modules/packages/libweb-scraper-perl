libweb-scraper-perl (0.38-2) unstable; urgency=medium

  [ Salvatore Bonaccorso ]
  * debian/control: Use HTTPS transport protocol for Vcs-Git URI

  [ gregor herrmann ]
  * debian/copyright: change Copyright-Format 1.0 URL to HTTPS.
  * debian/upstream/metadata: use HTTPS for GitHub URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-* headers for switch to salsa.debian.org

  [ gregor herrmann ]
  * debian/watch: use uscan version 4.

 -- Jelmer Vernooĳ <jelmer@debian.org>  Tue, 28 Jun 2022 22:21:30 +0100

libweb-scraper-perl (0.38-1.1) unstable; urgency=medium

  * Non maintainer upload by the Reproducible Builds team.
  * No source change upload to rebuild on buildd with .buildinfo files.

 -- Holger Levsen <holger@debian.org>  Tue, 05 Jan 2021 01:30:37 +0100

libweb-scraper-perl (0.38-1) unstable; urgency=medium

  [ gregor herrmann ]
  * Strip trailing slash from metacpan URLs.

  [ Salvatore Bonaccorso ]
  * Update Vcs-Browser URL to cgit web frontend

  [ gregor herrmann ]
  * Add debian/upstream/metadata
  * Import upstream version 0.38
  * Update debian/copyright:
    + Bump years of upstream and packaging copyright.
    + Drop comment about copyright, now there is a clear statement.
    + Drop section about removed third-party files.
  * Build-depend on libmodule-build-tiny-perl.
    Bump debhelper dependency accordingly.
  * debian/rules: drop manual removal of script.
    It doesn't get installed anymore.
  * Add a spelling patch.
  * Mark package as autopkgtest-able.
  * Declare compliance with Debian Policy 3.9.6.

 -- gregor herrmann <gregoa@debian.org>  Wed, 22 Oct 2014 20:42:24 +0200

libweb-scraper-perl (0.37-1) unstable; urgency=low

  [ gregor herrmann ]
  * debian/control: update {versioned,alternative} (build) dependencies.

  [ Salvatore Bonaccorso ]
  * Change Vcs-Git to canonical URI (git://anonscm.debian.org)
  * Change search.cpan.org based URIs to metacpan.org based URIs

  [ gregor herrmann ]
  * New upstream release.
  * debian/copyright: update copyright years, convert to Copyright Format
    1.0.
  * Set Standards-Version to 3.9.4 (no changes).
  * Add libhtml-treebuilder-libxml-perl to Build-Depends-Indep and
    Recommends.

 -- gregor herrmann <gregoa@debian.org>  Sat, 02 Mar 2013 19:32:49 +0100

libweb-scraper-perl (0.36-1) unstable; urgency=low

  * New upstream release.

 -- gregor herrmann <gregoa@debian.org>  Sun, 20 Nov 2011 16:13:08 +0100

libweb-scraper-perl (0.35-1) unstable; urgency=low

  [ Ansgar Burchardt ]
  * debian/control: Convert Vcs-* fields to Git.

  [ Salvatore Bonaccorso ]
  * debian/copyright: Replace DEP5 Format-Specification URL from
    svn.debian.org to anonscm.debian.org URL.

  [ gregor herrmann ]
  * New upstream release.
  * Update years of copyright for inc/Module/*.

 -- gregor herrmann <gregoa@debian.org>  Sat, 01 Oct 2011 17:15:54 +0200

libweb-scraper-perl (0.34-1) unstable; urgency=low

  * Initial release (closes: #530467).

 -- gregor herrmann <gregoa@debian.org>  Sat, 09 Apr 2011 22:31:00 +0200
